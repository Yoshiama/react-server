1. If you don't already have it, download and install NodeJS 8.9.3 or later. (Installing NodeJS will also install npm.)

2. This project favors Yarn, so make sure you have the latest.

  ```
  $ npm install -g yarn
  ```

3. Install the build and application dependencies.

  ```
  $ yarn install
  ```

4. Launch the server with this command

  ```
  $ yarn start
  ```