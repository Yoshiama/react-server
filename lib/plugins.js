import hapiAuthJWT2 from 'hapi-auth-jwt2';
import Inert from 'inert';

const plugins = [hapiAuthJWT2, Inert];

export default plugins;
