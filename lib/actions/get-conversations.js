import Boom from 'boom';
import { omit, get } from 'lodash';
import Room from '../models/room';
import User from '../models/user';
import RoomsManager from '../rooms-manager';

export default {
  handler: async ({ auth }) => {
    try {
      const userId = get(auth, 'credentials.id');
      const user = await User.findById(userId);
      const userManager = RoomsManager.findUserById(userId);

      if (!user) {
        return Boom.notFound('User not found');
      }

      if (!userManager) {
        return [];
      }

      const rooms = await Room.find({ _id: { $in: userManager.rooms } });

      return Promise.all(
        rooms.map(async room => ({
          id: room._id,
          ...omit(room.toObject(), [
            '_id',
            '__v',
            'anonymes',
            'kicked',
            'messages',
          ]),
          members: await RoomsManager.getMembers(room._id),
        })),
      );
    } catch (err) {
      console.error(err);
      return Boom.internal(err);
    }
  },
};
