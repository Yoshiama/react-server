import Boom from 'boom';
import { omit } from 'lodash';
import Joi from 'joi';
import Room from '../models/room';
import RoomsManager from '../rooms-manager';

export default {
  validate: {
    params: Joi.object({
      tag: Joi.string()
        .regex(/^[0-9a-zA-Z]{2,20}$/)
        .default('')
        .optional(),
    }),
  },
  handler: async ({ params }) => {
    try {
      let rooms = null;
      if (params.tag === '') {
        rooms = await Room.find();
      } else {
        rooms = await Room.find({
          tags: { $in: [RegExp(`.*${params.tag}.*`, 'i')] },
        });
      }

      return rooms.map(room => ({
        id: room._id,
        ...omit(room.toObject(), [
          '_id',
          '__v',
          'anonymes',
          'kicked',
          'messages',
        ]),
        members: RoomsManager.getMembers(room._id),
      }));
    } catch (err) {
      console.error(err);
      return Boom.internal(err);
    }
  },
};
