import Boom from 'boom';
import { omit } from 'lodash';
import Room from '../models/room';
import RoomsManager from '../rooms-manager';

export default {
  handler: async () => {
    try {
      const rooms = await Room.find();
      return Promise.all(
        rooms.map(async room => ({
          id: room._id,
          ...omit(room.toObject(), [
            '_id',
            '__v',
            'anonymes',
            'kicked',
            'messages',
          ]),
          members: await RoomsManager.getMembers(room._id),
        })),
      );
    } catch (err) {
      console.error(err);
      return Boom.internal(err);
    }
  },
};
