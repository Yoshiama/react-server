import Joi from 'joi';
import Boom from 'boom';

import { get, find } from 'lodash';
import User from '../models/user';
import Room from '../models/room';
import RoomsManager from '../rooms-manager';
import Anonyme from '../models/anonyme';

export default {
  validate: {
    params: Joi.object().keys({
      id: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/)
        .required(),
    }),
  },
  handler: async ({ params, auth }, h) => {
    try {
      const userId = get(auth, 'credentials.id');
      const user = await User.findById(userId);
      let room = await Room.findById(params.id);
      let found = false;
      let anonymeId = null;

      if (!user) {
        return Boom.notFound('User not found');
      } else if (!room) {
        return Boom.notFound('Room not found');
      }

      found = !!find(
        room.anonymes.toObject(),
        anonyme =>
          anonyme.user._id.toString() === userId && anonyme.admin !== true,
      );

      if (found) {
        const anoModel = await RoomsManager.getMember(anonymeId);
        room.anonymes.forEach((anonyme) => {
          if (anonyme.user._id.toString() === userId) {
            anonymeId = anonyme._id;
            Anonyme.findById(anonyme._id)
              .remove()
              .exec();

            anonyme.remove();
          }
        });
        room = RoomsManager.findById(params.id);
        const data = {
          type: 'LEAVE',
          data: {
            room: room.roomId,
            member: anoModel,
            message: '',
            date: Date.now,
          },
        };
        RoomsManager.deleteAnonyme(room._id, anonymeId);
        await RoomsManager.sendMessage(room, anoModel.anonymeId, data);
        return h.response({}).code(204);
      }
      return Boom.unauthorized(
        'You cannot leave this room if you are admin or not in there',
      );
    } catch (err) {
      console.error(err);
      return Boom.internal(err);
    }
  },
};
