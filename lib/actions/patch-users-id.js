import Joi from 'joi';
import Boom from 'boom';
import { omit, get } from 'lodash';
import User from '../models/user';

export default {
  validate: {
    params: Joi.object({
      id: Joi.string().required(),
    }),
    payload: Joi.object({
      email: Joi.string().email(),
      name: Joi.string().required(),
      age: Joi.number()
        .integer()
        .min(18),
      description: Joi.string().optional(),
    }),
  },
  handler: async ({ params, payload, auth }) => {
    try {
      if (get(auth, 'credentials.id') !== params.id) {
        return Boom.unauthorized();
      }

      const user = await User.findOne({ _id: params.id });

      if (!user) {
        return Boom.notFound();
      }

      user.email = payload.email;
      user.name = payload.name;
      user.age = payload.age;
      user.description = payload.description;

      const result = (await user.save()).toObject();
      return {
        id: result._id,
        ...omit(result, ['_id', '__v']),
      };
    } catch (err) {
      console.error(err);
      return Boom.internal(err);
    }
  },
};
