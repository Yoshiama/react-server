import getUsers from './actions/get-users';
import postUsers from './actions/post-users';
import getUsersId from './actions/get-users-id';
import patchUsersId from './actions/patch-users-id';
import postLogin from './actions/post-login';
import postMessages from './actions/post-messages';
import postJoinRooms from './actions/post-join-rooms';
import getRooms from './actions/get-rooms';
import getMembers from './actions/get-members';
import postRooms from './actions/post-rooms';
import deleteRooms from './actions/delete-rooms-id';
import postRoomsKickId from './actions/post-rooms-kick-id';
import postRoomsReveal from './actions/post-rooms-reveal';
import getRoomsTag from './actions/get-rooms-tag';
import postUsersPicture from './actions/post-users-picutre';
import getConversations from './actions/get-conversations';
import postRoomsLeave from './actions/post-rooms-leave';

const routes = [
  {
    method: 'POST',
    path: '/rooms/{id}/messages',
    config: { ...postMessages, auth: 'jwt' },
  },
  {
    method: 'GET',
    path: '/rooms/{id}/members',
    config: { ...getMembers, auth: 'jwt' },
  },
  {
    method: 'POST',
    path: '/rooms/{id}/join',
    config: { ...postJoinRooms, auth: 'jwt' },
  },
  {
    method: 'GET',
    path: '/rooms',
    config: { ...getRooms, auth: 'jwt' },
  },
  {
    method: 'POST',
    path: '/rooms',
    config: { ...postRooms, auth: 'jwt' },
  },
  {
    method: 'DELETE',
    path: '/rooms/{id}',
    config: { ...deleteRooms, auth: 'jwt' },
  },
  {
    method: 'POST',
    path: '/rooms/{id}/kick',
    config: { ...postRoomsKickId, auth: 'jwt' },
  },
  {
    method: 'POST',
    path: '/users',
    config: { ...postUsers, auth: false },
  },
  {
    method: 'GET',
    path: '/users',
    config: { ...getUsers, auth: 'jwt' },
  },
  {
    method: 'GET',
    path: '/users/{id}',
    config: { ...getUsersId, auth: 'jwt' },
  },
  {
    method: 'PATCH',
    path: '/users/{id}',
    config: { ...patchUsersId, auth: 'jwt' },
  },
  {
    method: 'POST',
    path: '/login',
    config: { ...postLogin, auth: false },
  },
  {
    method: 'GET',
    path: '/rooms/conversations',
    config: { ...getConversations, auth: 'jwt' },
  },
  {
    method: 'POST',
    path: '/rooms/{id}/reveal',
    config: { ...postRoomsReveal, auth: 'jwt' },
  },
  {
    method: 'POST',
    path: '/rooms/{id}/leave',
    config: { ...postRoomsLeave, auth: 'jwt' },
  },
  {
    method: 'GET',
    path: '/rooms/tag/{tag}',
    config: { ...getRoomsTag, auth: 'jwt' },
  },
  {
    method: 'POST',
    path: '/users/picture',
    config: {
      payload: {
        output: 'file',
        maxBytes: 1000 * 1000 * 2,
      },
      ...postUsersPicture,
      auth: 'jwt',
    },
  },
  {
    method: 'GET',
    config: { auth: 'jwt' },
    path: '/pictures/{filename}',
    handler: {
      file(request) {
        return request.params.filename;
      },
    },
  },
];

export default routes;
