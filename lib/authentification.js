import Joi from 'joi';

export const AUTH_SECRET = 'Jp9PTwyi9EhiZBupNhzSewtGDgef';

const JoiUUID = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
  name: Joi.string().required(),
  age: Joi.number()
    .integer()
    .min(18)
    .required(),
  description: Joi.string()
    .default('')
    .optional(),
});

export const validate = ({ id }) => {
  Joi.validate(id, JoiUUID);

  if (!id) {
    return { isValid: false };
  }
  return { isValid: true };
};
