import Hapi from 'hapi';
import io from 'socket.io';
import plugins from './plugins';
import SendMessage from './send-message';
import { AUTH_SECRET, validate } from './authentification';

import routes from './routes';

async function start() {
  const server = Hapi.server({
    port: 4000,
    routes: {
      cors: { origin: ['*'] },
      files: {
        relativeTo: `${process.cwd()}/pictures`,
      },
    },
  });

  try {
    await server.register(plugins);

    server.auth.strategy('jwt', 'jwt', {
      key: AUTH_SECRET,
      validate,
    });
    server.auth.default('jwt');

    server.route(routes);
    const ioServer = io(server.listener);
    SendMessage.setSocket(ioServer);
    ioServer.on('connection', (socket) => {
      socket.on('connected', (payload) => {
        SendMessage.connect(socket, payload);
      });
    });
    await server.start();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }

  console.log('Server running at:', server.info.uri);
}

start();
