import Boom from 'boom';
import jwt from 'jsonwebtoken';
import { AUTH_SECRET } from './authentification';

import RoomsManager from './rooms-manager';

class SendMessage {
  constructor() {
    this.socket = null;
  }

  setSocket(ioSocket) {
    this.socket = ioSocket;
  }

  getMessageFrom(roomId) {
    this.socket.on(roomId.toString(), this.sendMessage);
  }

  broadcastMessageTo(roomId, message) {
    this.socket.to(roomId.toString()).emit(roomId.toString(), message);
  }

  async getUserId(token) {
    const decode = await jwt.verify(token, AUTH_SECRET);
    if (!decode) return null;
    return decode.id;
  }

  async connect(socket, payload) {
    try {
      const userId = await this.getUserId(payload.auth);
      const user = RoomsManager.findUserById(userId);
      if (user) {
        Promise.all(
          user.rooms.map(async (roomId) => {
            const room = RoomsManager.findById(roomId);
            const anonyme = room.anonymes.filter(
              ano => ano.userId.toString() === userId,
            )[0];
            anonyme.ws = socket;
            const message = {
              type: 'CONNECT',
              data: {
                room: '',
                member: await RoomsManager.getMemberForUserId(roomId, userId),
                message: '',
                date: Date.now,
              },
            };
            message.data.roomId = roomId;
            socket.join(roomId, () => {
              socket.broadcast.to(roomId).emit(roomId, message);
            });
            socket.on(roomId, (data) => {
              this.sendMessage(roomId, data);
            });
          }),
        );
      } else {
        socket.emit('connected', { error: 'User not found' });
        return { error: 'User not found' };
      }

      socket.emit('connected', { result: 'ok' });
      return { result: 'ok' };
    } catch (err) {
      console.error(err);
      socket.emit({ error: 'internal error' });
      return Boom.internal(err);
    }
  }

  async sendMessage(roomId, payload) {
    try {
      const userId = await this.getUserId(payload.auth);
      payload.message = payload.message || '';
      // MESSAGE
      const room = RoomsManager.findById(payload.room);
      if (!room) {
        return Boom.notFound('Room not found');
      }

      const anonyme = room.anonymes.filter(
        ano => ano.userId.toString() === userId,
      )[0];

      if (anonyme) {
        const anoModel = await RoomsManager.getMember(anonyme.anonymeId);
        const data = {
          type: 'MESSAGE',
          data: {
            room: room.roomId,
            member: anoModel,
            message: payload.message,
            date: Date.now,
          },
        };
        await RoomsManager.sendMessage(room, anonyme.anonymeId, data);
        return { result: 'MESSAGE SENT' };
      }
      return Boom.unauthorized('Unauthorized');
    } catch (err) {
      console.error(err);
      return Boom.internal(err);
    }
  }
}

const sm = new SendMessage();

export default sm;
